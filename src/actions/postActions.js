export const deletePost = (id) => {
    return {
        type: 'DELETE_POST', 
        id
        // in ES6 id means id: id
    }
}