import React from 'react'

const About = () => {
    return (
        <div className="container">
            <h4 className="center">About</h4>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui accusantium, ullam cumque ea at perferendis ipsum vitae voluptatibus maiores blanditiis assumenda optio, molestiae a. Explicabo doloribus doloremque quae quas illum!</p>
        </div>
    ) 
}

export default About