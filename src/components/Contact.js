import React from 'react'

const Contact = (props) => {
    // Programic Redirect
    // setTimeout(() => {
    //     props.history.push("/about")
    // }, 2000)
    return (
        <div className="container">
            <h4 className="center">Contact</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione delectus dolore distinctio quibusdam eligendi dolor fugiat placeat. Velit qui, aperiam labore, assumenda sapiente quibusdam ex, et id hic alias reiciendis.</p>
        </div>
    )
}

export default Contact