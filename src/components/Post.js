import React, {Component} from 'react'
import { connect } from 'react-redux'
import { deletePost } from '../actions/postActions'

class Post extends Component {

    handleClick = () => {
        this.props.deletePost(this.props.post.id);
        // redirect to Home Page after deleting...
        this.props.history.push('/');
    }

    render () {
            console.log(this.props);
        const post = this.props.post ? (
            <div className="post">
                <h2 className="center">{this.props.post.title}</h2>
                <p>{this.props.post.body}</p>
                <div className="center">
                    <button className="btn grey" onClick={this.handleClick}>Delete Post</button>
                </div>
            </div>
        ) : (
            <div className="container center">Data is loading...</div>
        )


        return (
            <div className="container">
               { post }
            </div>
        )
    }

}

// mapStateToProps: get state from Central Store (reducer) and then map it to props in this component 
// 
const mapStateToProps = (state, ownProps) => {
    let id = ownProps.match.params.post_id;
    return {
       post: state.posts.find(post => post.id == id)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        // deletePost is a func
        deletePost: (id) => {
            dispatch(deletePost(id))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Post)