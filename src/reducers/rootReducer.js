const initState = {
    posts: [
        {id: 1, title: "sunt aut facere repellat provident occaecati excepturi optio reprehenderit", body: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus earum, sed error facilis laboriosam accusantium nostrum quidem perferendis deleniti provident inventore tempore necessitatibus, illo minus possimus aperiam cupiditate repudiandae? Id.'},
        {id: 2, title: "nesciunt quas odio", body: "ullam et saepe reiciendis voluptatem adipisci sit amet autem assumenda provident rerum culpa quis hic commodi nesciunt rem tenetur doloremque ipsam iure quis sunt voluptatem rerum illo velit"}
    ]
}

const rootReducer = (state = initState, action) => {
    if (action.type === 'DELETE_POST') {

        // take the post out if the post.id == action.id
        let newPosts = state.posts.filter(post => {
            return action.id !== post.id;
        });

        // ...state: take everthing from state and then modify it with newPosts for the posts.
        return {
            ...state, 
            posts: newPosts
        }
    }
    return state;
}

export default rootReducer